<!-- Sticky Footer -->
<footer class="sticky-footer">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright © <?php echo date('Y'); ?> | CoreCms by <a href="https://www.bfacore.com" target="_blank">support@bfacore.com</a></span>
        </div>
    </div>
</footer>

</div>
<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Kill session?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" if you want to terminate this session.</div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger" href="<?php echo $custdir; ?>/acp/logout.php">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo $custdir; ?>/assets/jquery/jquery.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/jquery-chained/jquery.chained.js"></script>
<script src="<?php echo $custdir; ?>/assets/bootbox/bootbox.all.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo $custdir; ?>/assets/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="<?php echo $custdir; ?>/assets/chart.js/Chart.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/jquery.dataTables.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/dataTables.bootstrap4.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/buttons.dataTables.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/buttons.html5.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/datatables/buttons.print.min.js"></script>

<script src="<?php echo $custdir; ?>/assets/jszip/dist/jszip.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/jspdf/jspdf.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo $custdir; ?>/acp/js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="<?php echo $custdir; ?>/assets/datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo $custdir; ?>/assets/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo $custdir; ?>/assets/datepicker/locales/bootstrap-datepicker.ro.min.js"></script>


<script>
    $(document).ready(function() {
        $('#dayli').datepicker({
                format: "dd-mm-yyyy",
                todayBtn: "linked",
                language: "en",
                todayHighlight: true
            }
        );

        $('#dayli2').datepicker({
                format: "dd-mm-yyyy",
                todayBtn: "linked",
                language: "en",
                todayHighlight: true
            }
        );
		
		

        $('#dataTable').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                //title: title,
                text: '<i class="fal fa-copy"></i>&nbsp;Copy as TEXT',
                extend: 'copyHtml5'
            } , {
                //title: title,
                filename: 'Raprot_' + new Date().toISOString().split('T')[0],
                text: '<i class="fal fa-file-csv"></i>&nbsp;Save as CSV',
                extend: 'csvHtml5',
                charset: 'UTF-8',
                bom: true
            } , {
                //title: title,
                filename: 'Raprot_' + new Date().toISOString().split('T')[0],
                text: '<i class="fal fa-file-excel"></i>&nbsp;Save as XLSX',
                extend: 'excelHtml5'
            } , {
                //title: title,
                filename: 'Raprot_' + new Date().toISOString().split('T')[0],
                text: '<i class="fal fa-file-pdf"></i>&nbsp;Save as PDF',
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'A3'
            } , {
                //title: title,
                text: '<i class="fal fa-print"></i>&nbsp;Print',
                extend: 'print',
                orientation: 'landscape',
                pageSize: 'A3'
            }]
        });
    });
</script>
</body>

</html>
