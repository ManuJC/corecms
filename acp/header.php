<?php
include('../config.php');

if($site_dir == '') {
    $custdir = '';
} elseif ($site_dir != '') 
{ 
    $custdir = '/'.$site_dir;
}

if(!isset($_SESSION['acp']))
{
    header("Location: $custdir/acp/login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="FrontDesk Manager">
    <meta name="author" content="Laurentiu Costache">

    <title><?php echo $site_name; ?> Manager</title>

    <!-- Bootstrap core CSS-->
    <link href="<?php echo $custdir; ?>/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<?php echo $custdir; ?>/assets/fontawesome/css/all.css" rel="stylesheet" type="text/css">
	<script src="<?php echo $custdir; ?>/acp/js/nightmode.js"></script>

    <!-- Date picker -->
    <link href="<?php echo $custdir; ?>/assets/datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $custdir; ?>/assets/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

    <!-- Select search -->
    <link href="<?php echo $custdir; ?>/assets/bootstrap-select/css/bootstrap-select.min.css" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="<?php echo $custdir; ?>/assets/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

	<!-- Wowhead tooltips -->
    <script>var whTooltips = {colorLinks: true, iconizeLinks: true, renameLinks: true};</script>
    <script src="https://wow.zamimg.com/widgets/power.js"></script>

    <!-- Custom styles for this template-->
    <link href="<?php echo $custdir; ?>/acp/css/sb-admin.css" rel="stylesheet">
</head>

<body id="page-top">

<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="<?php echo $custdir; ?>/acp/dashboard.php"><i class="fal fa-tasks"></i> <?php echo $site_name; ?> Manager</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">

    </form>
    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fad fa-user-cog"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <!-- <a class="dropdown-item" href="/acp/settings.php">Account settings</a>
                <div class="dropdown-divider"></div> -->
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/logout.php" data-toggle="modal" data-target="#logoutModal">Logout</a>
            </div>
        </li>
    </ul>

</nav>